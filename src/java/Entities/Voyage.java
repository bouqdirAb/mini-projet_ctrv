/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author BOUQDIR
 */
@Entity
public class Voyage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne
    private Conducteur conducteur;
    @ManyToOne
    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    @OneToOne
    private Type_Voyage tv;
    @OneToOne
    public Ville villeprov;
     @OneToOne
    public Ville villedest;

    public Ville getVilleprov() {
        return villeprov;
    }

    public void setVilleprov(Ville villeprov) {
        this.villeprov = villeprov;
    }

    public Ville getVilledest() {
        return villedest;
    }

    public void setVilledest(Ville villedest) {
        this.villedest = villedest;
    }
    private int places;
    private float prix;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public Conducteur getConducteur() {
        return conducteur;
    }

    public void setConducteur(Conducteur conducteur) {
        this.conducteur = conducteur;
    }

    public Type_Voyage getTv() {
        return tv;
    }

    public void setTv(Type_Voyage tv) {
        this.tv = tv;
    }

    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
public Voyage(Ville vd, Ville vp, Conducteur cd,  int places, float prix, String date,Type_Voyage tv){
    this.conducteur=cd;
    this.date=date;
    this.places=places;
    this.prix=prix;
    this.villedest=vd;
    this.villeprov=vp;
    this.tv=tv;
    
}
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Voyage)) {
            return false;
        }
        Voyage other = (Voyage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Voyage[ id=" + id + " ]";
    }
    
}
