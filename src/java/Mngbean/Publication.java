/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mngbean;

import Dao.DaoTraitement;
import Entities.Voyage;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author BOUQDIR
 */
@ManagedBean
@SessionScoped

public class Publication {

   private String log;
   private String pass;
  private String villdest;
    private String villprov;
      private String typev;

    public String getVilldest() {
        return villdest;
    }

    public void setVilldest(String villdest) {
        this.villdest = villdest;
    }

    public String getVillprov() {
        return villprov;
    }

    public void setVillprov(String villprov) {
        this.villprov = villprov;
    }

    public String getTypev() {
        return typev;
    }

    public void setTypev(String typev) {
        this.typev = typev;
    }


    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
  private Voyage voyage;
  
    public Voyage getVoyage() {
        return voyage;
    }

    public void setVoyage(Voyage voyage) {
        this.voyage = voyage;
    }

    
    
    public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }
     
    public void click() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
         
        requestContext.update("form:display");
        requestContext.execute("PF('dlg').show()");
    }
    public String pub(){
        DaoTraitement P = new DaoTraitement();
        voyage.setConducteur(P.getCondByLP(log, pass));
        voyage.setVilledest(P.getVilleByName(villdest));
        voyage.setVilleprov(P.getVilleByName(villprov));
         voyage.setTv(P.getTVByName(typev));
        P.save(voyage);
        if(voyage!=null)
        return "index";
        else
            return "publication";
    }
}