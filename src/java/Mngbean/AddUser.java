/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mngbean;

import Dao.DaoTraitement;
import Entities.*;

import Entities.User;
import java.sql.SQLException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author BOUQDIR
 */
@ManagedBean
@RequestScoped
public class AddUser {

     private Client client;
     private Conducteur cond;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Conducteur getCond() {
        return cond;
    }

    public void setCond(Conducteur cond) {
        this.cond = cond;
    }

    
  
    
     public String addclnt() throws SQLException{
        
        DaoTraitement P = new DaoTraitement();
          P.save(client);
        return "profil";
     }
     public String addcond() throws SQLException{
        
        DaoTraitement P = new DaoTraitement();
          P.save(cond);
        return "profil";
     }
    
}
