/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mngbean;

import Dao.DaoTraitement;
import Entities.User;
import java.sql.SQLException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author BOUQDIR
 */
@ManagedBean
@SessionScoped
public class MnLogin {

     private String log;
        private String pass;

          private User us;

    public User getUs() {
        return us;
    }

    public void setUs(User us) {
        this.us = us;
    }
          
    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
        
    public String authentif() {
         
    DaoTraitement P = new DaoTraitement();
    us=P.findUser(log, pass);
        if(us!=null)
             return "profil";
       else
            return "inscription";
    }
    
    
}
