/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Entities.*;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author BOUQDIR
 */
public class DaoTraitement {
    
    public final SessionFactory sessionFactory = new Configuration().configure("/hibernate.cfg.xml").buildSessionFactory();
         
    public final   Session S = sessionFactory.openSession();
    
    public void Useradd(User us){
        
        S.beginTransaction();
        S.save(us);
        S.getTransaction().commit();
        
    }
    public void save(Object obj){
        
         S.beginTransaction();
        S.save(obj);
        S.getTransaction().commit();
        
    }
    public void Update(Object obj){
        
        S.beginTransaction();
        S.merge(obj);
        S.getTransaction().commit();
        
    }
    public Object find(Object obj,Long id){
        
        return (Object)S.get(obj.getClass(),id);
    }
    public User findUser(String log,String pass){
        
         
        String hql="FROM User U WHERE U.login='"+ log +"' and U.password='"+ pass +"' ";
		Query query = S.createQuery(hql);
		List<User> results = query.list();

	   User	us=results.get(0);
		
        return us;
    }
    public Conducteur getCondByLP(String log,String pass){
        
        
         String hql="FROM Conducteur CD WHERE CD.login='"+ log +"' and CD.password='"+ pass +"' ";
		Query query = S.createQuery(hql);
		List<Conducteur> results = query.list();

	   Conducteur cd=results.get(0);
        
        return cd;
    }
     public Client getClntByLP(String log,String pass){
        
        
         String hql="FROM Client C WHERE C.login='"+ log +"' and C.password='"+ pass +"' ";
		Query query = S.createQuery(hql);
		List<Client> results = query.list();

	   Client cl=results.get(0);
        
        return cl;
    }
     public Ville getVilleByName(String nom){
         
         String hql="FROM Ville V WHERE V.ville='"+ nom +"' ";
		Query query = S.createQuery(hql);
		List<Ville> results = query.list();

	   Ville v=results.get(0);
        
        return v;
         
         
     }
     public Type_Voyage getTVByName(String nom){
         
          String hql="FROM Type_Voyage TV WHERE TV.type='"+ nom +"' ";
		Query query = S.createQuery(hql);
		List<Type_Voyage> results = query.list();

	   Type_Voyage tv=results.get(0);
        
        return tv;
         
     }
     public static void main(String[] args){
        
         DaoTraitement T = new DaoTraitement();
        
        User u = new User("aamar","karim","0666524782","kamar","kamar","kamar@gmail.com","01/03/1990");
        Client cl = new Client("aamar","karim","0666524782","kamar","kamar","kamar@gmail.com","01/03/1990");
        Conducteur cd = new Conducteur("aamar","karim","0666524782","kamar","kamar","kamar@gmail.com","01/03/1990");
        T.save(cl);
                T.save(cd);
        u.setCond(cd);
        u.setClt(cl);
        T.Useradd(u);
    }
}
